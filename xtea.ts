// Adapted from https://github.com/simplito/xtea-js

const ROUNDS = 32;
const DELTA = 0x9e3779b9;

function encipher(v: Uint32Array, k: Uint32Array) {
  let y = v[0];
  let z = v[1];
  let sum = 0;

  for (let i = 0; i < ROUNDS; i++) {
    y += (((z << 4) ^ (z >>> 5)) + z) ^ (sum + k[sum & 3]);
    sum += DELTA;
    z += (((y << 4) ^ (y >>> 5)) + y) ^ (sum + k[(sum >> 11) & 3]);
  }

  v[0] = y;
  v[1] = z;
}

export function encrypt(data: Buffer, key: Buffer) {
  var length = data.length;
  var pad = 8 - (length & 7);

  if (pad !== 0) {
    data = Buffer.concat([data, Buffer.alloc(pad, 0xff)]);
  }

  var out = Buffer.alloc(length + pad);
  var k = new Uint32Array(4);
  var v = new Uint32Array(2);

  for (var i = 0; i < 4; ++i) {
    k[i] = key.readUInt32LE(i * 4);
  }

  for (let i = 0; i < length; i += 8) {
    v[0] = data.readUInt32BE(i);
    v[1] = data.readUInt32BE(i + 4);

    encipher(v, k);

    out.writeUInt32BE(v[0], i);
    out.writeUInt32BE(v[1], i + 4);
  }

  return out;
}
