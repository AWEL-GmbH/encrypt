#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import path from 'path';
import intelhex from 'node-intelhex';
import { encrypt } from './xtea';
import { version } from './package.json';

type Options = {
  key?: string;
  outFile?: string;
};

const MAX_BYTES_PER_LINE = 16;
intelhex.setLineBytes(MAX_BYTES_PER_LINE);

program
  .name('encrypt')
  .version(version, '-v, --version')
  .arguments('<source-file>')
  .option('-k, --key <key>', 'encryption key used to encrypt <source-file>')
  .option(
    '-o, --out-file <file>',
    'write output to <file> (default: encrypted.<source-file>)',
  )
  .action(actionHandler);

program.parse(process.argv);
if (program.args.length === 0) {
  fatalError('source file is required.');
}

async function actionHandler(source: string, opts: Options) {
  if (!fs.existsSync(source)) return fatalError(`${source} does not exists.`);
  if (!opts.key) return fatalError('encryption key is required.');

  const { data, address } = await intelhex.readFile(source);
  const encryptedData = encrypt(data, Buffer.from(opts.key));
  const outFile = getOutFile(source, opts.outFile);

  return writeFile(outFile, address, encryptedData);
}

function writeFile(path: string, address: number, data: Buffer) {
  const reader = intelhex.bufferReader(address, data);
  let output = '';

  while (!reader.eof()) output += `${reader.getNextRecord().toUpperCase()}\r\n`;

  fs.writeFile(path, output, err => {
    if (err) throw err;

    console.log(`Succesfully emitted encrypted source to ${path}`);
    process.exit(0);
  });
}

function getOutFile(srcFile: string, outFile: string | undefined) {
  if (outFile) return outFile;

  const basename = path.basename(srcFile);
  return path.join(path.dirname(srcFile), `encrypted.${basename}`);
}

function fatalError(msg: string) {
  console.error(`error: ${msg}`);
  process.exit(1);
}
